import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArztFormComponent } from './arzt-form.component';

describe('ArztFormComponent', () => {
  let component: ArztFormComponent;
  let fixture: ComponentFixture<ArztFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArztFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArztFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ArztComponent } from '../arzt/arzt.component';
import { ArztModel } from '../arzt-model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class NewArztService {
  api = 'http://localhost:44300/api/Arzt/';
  
  constructor(private http: HttpClient) {}

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      alert('An error occurred:' + error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      alert(`${error.error.ExceptionMessage}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  insertArzt(data: string): Observable<ArztModel> {
    return this.http.post<ArztModel>(this.api, data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  } 
}

@Component({
  selector: 'app-arzt-form',
  templateUrl: './arzt-form.component.html',
  styleUrls: ['./arzt-form.component.css']
})
export class ArztFormComponent {
  model = new ArztModel("","");

  constructor(private nas : NewArztService, private arztcomponent : ArztComponent) { }


  submitted = false;
  onSubmit() {
    this.submitted = true;
  }  

  newArzt() {
    this.nas.insertArzt(JSON.stringify(this.model)).subscribe((data)=>{
      alert("Arzt wurde erfolgreich hinzugefügt!");
    // location.reload();
    });
  }
}

import { Component, OnInit, Injectable, Input } from '@angular/core';
import { TerminModel } from '../termin-model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ArztTerminService {
  api = 'http://localhost:44300/api/Termin/Arzt/';
  list: any;
  id: any;

  constructor(private http: HttpClient) {}

  getTermineByArzt(id: number): Observable<TerminModel[]> {
    this.id = id;
    return this.http.get<TerminModel[]>(`${this.api}`+this.id);
  }
}

@Component({
  selector: 'app-show-termine-by-arzt',
  templateUrl: './show-termine-by-arzt.component.html',
  styleUrls: ['./show-termine-by-arzt.component.css']
})
export class ShowTermineByArztComponent implements OnInit {
  private termine: TerminModel[];
  @Input()id: number;

  constructor(private arztterminservice : ArztTerminService) {}

  getTerminDetails(id: number) {
    this.arztterminservice.getTermineByArzt(this.id).subscribe((terminDetail : TerminModel[]) => {
      this.termine = terminDetail;
    });
  }

  ngOnInit() {
    this.arztterminservice.getTermineByArzt(this.id).subscribe(terminDetails => this.termine = terminDetails);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTermineByArztComponent } from './show-termine-by-arzt.component';

describe('ShowTermineByArztComponent', () => {
  let component: ShowTermineByArztComponent;
  let fixture: ComponentFixture<ShowTermineByArztComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowTermineByArztComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTermineByArztComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

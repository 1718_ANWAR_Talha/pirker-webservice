import { Component, Injectable } from '@angular/core';
import { TerminModel } from '../termin-model';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, timeout } from 'rxjs/operators';
import { ArztModel } from '../arzt-model';

@Injectable()
export class NewTerminService {
  api = 'http://localhost:44300/api/Termin';
  getArztIdsApi = 'http://localhost:44300/api/Arzt/IDs'; //API for Arzt-IDs only
  getArztApi = 'http://localhost:44300/api/Arzt'; // API for Arzt-Name

  constructor(private http: HttpClient) {}

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      alert('An error occurred:' + error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      alert(`${error.error.ExceptionMessage}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  getArztIDs(): Observable<number[]> {
    return this.http.get<number[]>(this.getArztIdsApi, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getArzt(): Observable<ArztModel[]> {
    return this.http.get<ArztModel[]>(this.getArztApi, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  insertTermin(data: string): Observable<TerminModel> {
    return this.http.post<TerminModel>(this.api, data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  } 
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};
@Component({
  selector: 'app-termin-form',
  templateUrl: './termin-form.component.html',
  styleUrls: ['./termin-form.component.css']
})
export class TerminFormComponent {
  selectIDs: number[];
  arzts: ArztModel[];
  model = new TerminModel("",null,null,null);
  insertTermin: TerminModel;

  constructor(private nts : NewTerminService) { }

  ngOnInit() {
    this.nts.getArztIDs().subscribe(x => {
      this.selectIDs = x;
    });
    this.nts.getArzt().subscribe(x => {
      this.arzts = x;
    });
  }

  newTermin() {
    this.nts.insertTermin(JSON.stringify(this.model)).subscribe(it => {
      this.insertTermin = it;
      alert("Termin wurde erfolgreich hinzugefügt!");
     // location.reload();
    });
  }

  get diagnostic() { return JSON.stringify(this.model); }
}
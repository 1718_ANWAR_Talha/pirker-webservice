import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArztDetailComponent } from './arzt-detail.component';

describe('ArztDetailComponent', () => {
  let component: ArztDetailComponent;
  let fixture: ComponentFixture<ArztDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArztDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArztDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

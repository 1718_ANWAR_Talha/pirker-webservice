import { Component, OnInit, Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ArztModel } from '../arzt-model';

@Injectable()
export class ArztDetailService {
  api = 'http://localhost:44300/api/Arzt/';
  id: any;  

  constructor(private http: HttpClient) {}

  getArztDetail(id: number): Observable<ArztModel> {
    this.id = id;
    return this.http
      .get<ArztModel>(`${this.api}`+this.id);
  }
}

@Component({
  selector: 'app-arzt-detail',
  templateUrl: './arzt-detail.component.html',
  styleUrls: ['./arzt-detail.component.css']
})
export class ArztDetailComponent implements OnInit {
  private arzt: ArztModel;
  @Input()id: number;
  editable: boolean;
  private arztToEdit: ArztModel;

  constructor(private arztservice : ArztDetailService) { }

  getArztDetails(id: number) {
    this.arztservice.getArztDetail(this.id).subscribe((arztDetail:ArztModel) => {
      this.arzt = arztDetail;
    });
  }

  ngOnInit() {
    this.arztservice.getArztDetail(this.id).subscribe(arztDetails => this.arzt = arztDetails);
  }  

  editArzt(selected: ArztModel) {
    this.editable = true;
    this.arztToEdit = selected;
  }
}

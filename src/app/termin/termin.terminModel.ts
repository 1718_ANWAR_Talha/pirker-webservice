import { identifierModuleUrl } from "@angular/compiler";
import { Time } from "@angular/common";

export class TerminModel {
    Id: number;     
    Patientname: string;
    Beginn: Date;
    Uhrzeit: Time;
    Arzt_Id: number;
}
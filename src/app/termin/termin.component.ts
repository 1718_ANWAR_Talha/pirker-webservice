import { Component, OnInit, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { TerminModel } from './termin.terminModel';
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class TerminService {
  api = 'http://localhost:44300/api/Termin/';
  list: any;

  constructor(private http: HttpClient) {}

  loadAll(): Observable<TerminModel[]> {
    this.list = this.http.get<TerminModel[]>(`${this.api}`);
    return this.list;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      alert('An error occurred:' + error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      alert(`${error.error.ExceptionMessage}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  deleteTermin (id: number): Observable<{}> {
    const url = `${this.api}/${id}`;
    return this.http.delete(url, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
}

@Component({
  selector: 'app-termin',
  templateUrl: './termin.component.html',
  styleUrls: ['./termin.component.css']
})
export class TerminComponent implements OnInit {
  title = 'Termine';
  private terminList: TerminModel[];
  private selectedTermin: TerminModel;
  terminForm: boolean;

  constructor(private terminservice : TerminService) { }

  ngOnInit() {
    this.terminservice.loadAll().subscribe(terminlist => this.terminList = terminlist);
  }

  selectTerminDetails(selected: TerminModel) {
    this.selectedTermin = selected;
  }

  deleteTermin(selected: TerminModel) {
    this.terminservice.deleteTermin(selected.Id).subscribe((data)=>{
      alert("Termin wurde erfolgreich gelöscht!");
     // location.reload();
    });
  }

  openTerminForm() {
    this.terminForm = true;
  }
}

import { Component, OnInit, Injectable, Input } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { TerminModel } from '../termin-model';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ArztModel } from '../arzt-model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class EditTerminService {
  api = 'http://localhost:44300/api/Termin/';
  getArztApi = 'http://localhost:44300/api/Arzt'; // API for Arzt-Name
  id: any;

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      alert('An error occurred:' + error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      alert(`${error.error.ExceptionMessage}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  getArzt(): Observable<ArztModel[]> {
    return this.http.get<ArztModel[]>(this.getArztApi, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  updateTermin(id: number, data: string): Observable<TerminModel> {
    this.id = id;
    return this.http
      .put<TerminModel>(`${this.api}` + this.id, data, httpOptions)
        .pipe(
          catchError(this.handleError)
        );
  }
}

@Component({
  selector: 'app-edittermin',
  templateUrl: './edittermin.component.html',
  styleUrls: ['./edittermin.component.css']
})
export class EditterminComponent implements OnInit {
  @Input()terminToEdit: TerminModel;
  @Input()id: number;
  arzts: ArztModel[];
  model = new TerminModel('', null, null, null);
  terminEdited: TerminModel;
  constructor(private ets: EditTerminService) { }

  ngOnInit() {
    this.model = this.terminToEdit;
    this.ets.getArzt().subscribe(x => {
      this.arzts = x;
    });
  }

  editTermin() {
    this.ets.updateTermin(this.id, JSON.stringify(this.model)).subscribe(x => {
      this.terminEdited = x;
      alert("Termin wurde aktualisiert!");
     // location.reload();
    });
  }
}
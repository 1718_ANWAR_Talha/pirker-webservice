import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditterminComponent } from './edittermin.component';

describe('EditterminComponent', () => {
  let component: EditterminComponent;
  let fixture: ComponentFixture<EditterminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditterminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditterminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

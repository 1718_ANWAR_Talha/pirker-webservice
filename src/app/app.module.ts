import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArztComponent, ArztService } from './arzt/arzt.component';
import { ArztDetailComponent, ArztDetailService } from './arzt-detail/arzt-detail.component';
import { ShowTermineByArztComponent } from './show-termine-by-arzt/show-termine-by-arzt.component';
import { MainComponent } from './main/main.component';
import { TerminComponent, TerminService } from './termin/termin.component';
import { TerminDetailComponent, TerminDetailService } from './termin-detail/termin-detail.component';
import { ArztTerminService } from './show-termine-by-arzt/show-termine-by-arzt.component';
import { HttpClientModule } from '@angular/common/http';
import { ArztFormComponent, NewArztService } from './arzt-form/arzt-form.component';
import { TerminFormComponent, NewTerminService } from './termin-form/termin-form.component';
import { EditarztComponent, EditArztService } from './editarzt/editarzt.component';
import { EditterminComponent, EditTerminService } from './edittermin/edittermin.component';

@NgModule({
  declarations: [
    AppComponent,
    ArztComponent,
    ArztDetailComponent,
    ShowTermineByArztComponent,
    MainComponent,
    TerminComponent,
    TerminDetailComponent,
    ArztFormComponent,
    TerminFormComponent,
    EditarztComponent,
    EditterminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ArztService, ArztDetailService, TerminService, TerminDetailService, ArztTerminService, NewArztService, NewTerminService, EditArztService, EditTerminService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArztComponent } from './arzt.component';

describe('ArztComponent', () => {
  let component: ArztComponent;
  let fixture: ComponentFixture<ArztComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArztComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArztComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

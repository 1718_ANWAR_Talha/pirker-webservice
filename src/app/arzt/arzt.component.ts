import { Component, OnInit, Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { ArztModel } from '../arzt-model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class ArztService {
  api = 'http://localhost:44300/api/Arzt/';
  list: any;

  constructor(private http: HttpClient) {}

  loadAll(): Observable<ArztModel[]> {
    this.list = this.http.get<ArztModel[]>(`${this.api}`);
    return this.list;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      alert('An error occurred:' + error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      alert(`${error.error.ExceptionMessage}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  deleteArzt (id: number): Observable<{}> {
    const url = `${this.api}/${id}`;
    return this.http.delete(url, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
}

@Component({
  selector: 'app-arzt',
  templateUrl: './arzt.component.html',
  styleUrls: ['./arzt.component.css']
})
export class ArztComponent implements OnInit {
  title = 'Ärzte';
  private arztList: ArztModel[];
  private selectedArzt: ArztModel;
  private checked: number;
  private arztForm: boolean;

  constructor(private arztservice : ArztService) { }

  ngOnInit() {
    this.arztservice.loadAll().subscribe(arztlist => this.arztList = arztlist);
  }

  selectArztDetails(selected: ArztModel) {
    this.checked = 1;
    this.selectedArzt = selected;
  }

  selectArztTermine(selected: ArztModel) {
    this.checked = 2;
    this.selectedArzt = selected;
  }
  
  deleteArzt(selected: ArztModel) {
    this.arztservice.deleteArzt(selected.Id).subscribe((data)=>{
      alert("Arzt wurde erfolgreich gelöscht!");
     // location.reload();
    });
  }

  openArztForm() {
    this.arztForm = true;
  }

  getArztList () {
    return this.arztList;
  }
}

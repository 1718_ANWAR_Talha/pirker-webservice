import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArztComponent } from './arzt/arzt.component';
import { MainComponent } from './main/main.component';
import { TerminComponent } from './termin/termin.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent, 
  },
  {
    path: 'arzt',
    component: ArztComponent
  },
  {
    path: 'termin',
    component: TerminComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

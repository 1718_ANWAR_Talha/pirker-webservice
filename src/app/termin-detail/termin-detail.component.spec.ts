import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminDetailComponent } from './termin-detail.component';

describe('TerminDetailComponent', () => {
  let component: TerminDetailComponent;
  let fixture: ComponentFixture<TerminDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerminDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

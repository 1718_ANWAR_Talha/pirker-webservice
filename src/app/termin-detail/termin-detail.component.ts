import { Component, OnInit, Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TerminModel } from '../termin-model';

@Injectable()
export class TerminDetailService {
  api = 'http://localhost:44300/api/Termin/';
  id: any;

  constructor(private http: HttpClient) {}

  getTerminDetail(id: number): Observable<TerminModel> {
    this.id = id;
    return this.http
      .get<TerminModel>(`${this.api}`+this.id);
  }
}

@Component({
  selector: 'app-termin-detail',
  templateUrl: './termin-detail.component.html',
  styleUrls: ['./termin-detail.component.css']
})
export class TerminDetailComponent implements OnInit {
  private termin: TerminModel;
  @Input()id: number;
  editable: boolean;
  terminToEdit: TerminModel;

  constructor(private terminservice : TerminDetailService) {}

  getTerminDetails(id: number) {
    this.terminservice.getTerminDetail(this.id).subscribe((terminDetail : TerminModel) => {
      this.termin = terminDetail;
    });
  }

  ngOnInit() {
    this.terminservice.getTerminDetail(this.id).subscribe(terminDetails => this.termin = terminDetails);
  }

  editTermin(selected: TerminModel) {
    this.editable = true;
    this.terminToEdit = selected;
  }
}
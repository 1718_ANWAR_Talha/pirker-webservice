import { Component, OnInit, Input, Injectable } from '@angular/core';
import { ArztModel } from '../arzt-model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class EditArztService {
  api = 'http://localhost:44300/api/Arzt/';
  id: any;

  constructor(private http: HttpClient) { }

  updateArzt(id: number, data: string): Observable<ArztModel> {
    this.id = id;
    return this.http
      .put<ArztModel>(`${this.api}` + this.id, data, httpOptions);
  }
}

@Component({
  selector: 'app-editarzt',
  templateUrl: './editarzt.component.html',
  styleUrls: ['./editarzt.component.css']
})
export class EditarztComponent implements OnInit {
  @Input()arztToEdit: ArztModel;
  @Input()id: number;
  model = new ArztModel('', '');
  arztEdited: ArztModel;
  constructor(private editArztService: EditArztService) { }

  ngOnInit() {
    this.model = this.arztToEdit;
  }

  cancel() {
    
  }

  editArzt() {
    this.editArztService.updateArzt(this.id, JSON.stringify(this.model)).subscribe(x => {
      this.arztEdited = x;
      alert("Arzt wurde aktualisiert!");
     // location.reload();
    });
  }
}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarztComponent } from './editarzt.component';

describe('EditarztComponent', () => {
  let component: EditarztComponent;
  let fixture: ComponentFixture<EditarztComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarztComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarztComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

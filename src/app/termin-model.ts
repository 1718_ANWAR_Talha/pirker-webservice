import { Time } from "@angular/common";

export class TerminModel {
    Id: number;     
    Patientname: string;
    Beginn: Date;
    Uhrzeit: Time;
    Arzt_Id: number;
    
    constructor(
        Patientname,
        Beginn,
        Uhrzeit,
        Arzt_Id
    ) {}
}